# troy main build image

Full development image with Bitbucket Pipelines support, AWS CLI, JDK 11, Maven, Git, jq.

# build

to build and publish run

  docker build -t pipelines-awscli-jdk-11-maven-compact .
  docker login
  docker tag pipelines-awscli-jdk-11-maven-compact troygmbh/pipelines-awscli-jdk-11-maven-compact:latest
  docker push troygmbh/pipelines-awscli-jdk-11-maven-compact

# links

Published here:
https://hub.docker.com/repository/docker/troygmbh/pipelines-awscli-jdk-11-maven-compact/general

Based on
* https://github.com/carlossg/docker-maven/blob/26ba49149787c85b9c51222b47c00879b2a0afde/openjdk-11-slim/Dockerfile
* https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html
